package main

import (
	"context"
	"log"
	"time"

	"github.com/jaslife1/uniuri"

	"github.com/micro/go-micro/metadata"
	pb_building "gitlab.com/lazybasterds/building-service/proto/building"
	pb_image "gitlab.com/lazybasterds/image-service/proto/image"
)

const (
	defaultDurationLength = 5 * time.Second
)

func getImage(requestID, imageID string) (interface{}, error) {
	// Call to the grpc
	log.Println("Query - getImage, RequestID: ", requestID, " ImageID: ", imageID)
	ctx := metadata.NewContext(context.Background(), map[string]string{
		"requestID": requestID,
	})
	duration := time.Now().Add(defaultDurationLength)
	ctx, cancel := context.WithDeadline(ctx, duration)
	defer cancel()
	resp, err := imageServiceClient.GetImage(ctx, &pb_image.Image{Id: imageID})

	if err != nil {
		log.Printf("FAIL: Query - getImage, RequestID: %s, Message: Did not get the image\n", requestID)
		log.Println(err)
		return nil, err
	}
	log.Printf("Response: RequestID: %s, Result: {Id:%s, Filename: %s} *omitting the data*", requestID, resp.Image.Id, resp.Image.Filename)
	return resp.Image, nil
}

func getFloor(floorID string) (interface{}, error) {
	// Call to the grpc
	requestID := uniuri.New()
	log.Println("Query - getFloor, RequestID: ", requestID, " FloorID: ", floorID)
	ctx := metadata.NewContext(context.Background(), map[string]string{
		"requestID": requestID,
	})
	duration := time.Now().Add(defaultDurationLength)
	ctx, cancel := context.WithDeadline(ctx, duration)
	defer cancel()
	resp, err := buildingServiceClient.GetFloor(ctx, &pb_building.Floor{Id: floorID})

	if err != nil {
		log.Printf("FAIL: Query - getFloor, RequestID: %s, Message: Did not get the image\n", requestID)
		log.Println(err)
		return nil, err
	}
	log.Printf("Response: RequestID: %s, Result: %s", requestID, resp)
	return resp.Floor, nil
}
