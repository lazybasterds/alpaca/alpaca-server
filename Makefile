build:
	docker build -t alpaca_server .

run:
	docker run \
		-e MICRO_REGISTRY=mdns \
		-p 4000:4000 \
		alpaca_server

release:
	docker build -t registry.gitlab.com/lazybasterds/alpaca/alpaca-server:0.1.4 .
	docker push registry.gitlab.com/lazybasterds/alpaca/alpaca-server:0.1.4